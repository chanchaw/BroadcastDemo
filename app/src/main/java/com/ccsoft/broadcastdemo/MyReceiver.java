package com.ccsoft.broadcastdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {
    private static final String TAG = "chanchaw";

    @Override
    public void onReceive(Context context, Intent intent) {
        MainActivity mainActivity = ((MyApplication) context.getApplicationContext()).mainActivity;
        mainActivity.etReceivedBroadcast.append("broadcast: "+intent.getAction()+"\n");
        String msg = intent.getStringExtra("msg");

        Toast.makeText(context, "接收到了！", Toast.LENGTH_LONG).show();
        Log.i(TAG,"接收到广播的消息了！");
    }
}
